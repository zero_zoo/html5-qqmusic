$(function () {
    var lastVolume = 0.5
    // 复选框选择功能
    $(".checkbox").click(function () {
        $(this).toggleClass("checked")
    })
    // 复选框全选功能
    $("#checkbox-all").click(function () {
        if ($(this).hasClass("checked")) {
            $(".checkbox").addClass("checked")
        } else {
            $(".checkbox").removeClass("checked")
        }
    })
    // 下方播放按钮
    $(".btn-play").click(function () {
        $(this).toggleClass("play")
        if ($(this).hasClass("play")) {
            $("#playerAudio")[0].play()
        } else {
            $("#playerAudio")[0].pause()
        }
        updateProgress()
    })
    // 列表播放按钮 切歌
    $(".list-menu .play,.songlist-name a").click(function () {
        const parent = $(this).parents("tr")
        const url = parent.data("src")
        const name = parent.data("name")
        const author = parent.data("author")
        const cover = parent.data("cover")
        const album = parent.data("album")
        const lyric = parent.data("lyric")
        console.log("切歌：" + name + "-" + author + ":" + url + "," + cover)
        $("#playerAudio").attr("src", url)
        $("#playerAudio")[0].play();
        $(".title-name").html(name)
        $(".title-author").html(author)
        $(".info-cover img").attr("src", cover)
        $(".bg").attr("style", "background-image: url(" + cover + ");")
        $(".info-name a").html(name)
        $(".info-author a").html(author)
        $(".info-album a").html(album)
        loadLyric(lyric)
        updateProgress()
    })
    // 进度条
    $(".music-progress").click(function () {
        const audio = $("#playerAudio")[0]
        audio.currentTime = audio.duration * (event.offsetX / $(this).width())
        updateProgress()
    })
    // 音量条
    $(".player-voice").click(function () {
        console.log(event.offsetX)
        const audio = $("#playerAudio")[0]
        audio.volume = (event.offsetX / $(this).width())
        if (audio.volume > 0) $(".btn-voice").removeClass("mute")
        updateProgress()
    })
    // 静音按钮
    $(".btn-voice").click(function () {
        const audio = $("#playerAudio")[0]
        $(this).toggleClass("mute")
        if ($(this).hasClass("mute")) {
            lastVolume = audio.volume
            audio.volume = 0
        } else {
            audio.volume = lastVolume
        }
        updateProgress()
    })
    // 同步播放器状态
    $("#playerAudio")[0].onplay = function () {
        $(".btn-play").addClass("play")
    }
    $("#playerAudio")[0].onpause = function () {
        $(".btn-play").removeClass("play")
    }
    updateProgress()
    // 设置每秒更新进度条
    setInterval(updateProgress, 1000);
})
// 更新进度条
function updateProgress() {
    const audio = $("#playerAudio")[0];
    $("#musicPlayProgress").width((audio.currentTime / audio.duration) * 100 + "%")
    $(".music-info-time").html(formatSeconds(audio.currentTime) + " / " + formatSeconds(audio.duration ? audio.duration : 0))
    $("#playerVoiceProgress").width(audio.volume * 100 + "%")

    // 歌词位置
    const lyricAmount = $(".lyric-inner p").length
    const currentLine = Math.floor(lyricAmount * (audio.currentTime / audio.duration))
    $(".lyric-inner").animate({
        top: -(currentLine * 34) + 68 + "px"
    })
    $(".lyric-inner p").removeClass("on").eq(currentLine).addClass("on")

}
// 加载歌词
function loadLyric(url) {
    $(".lyric-inner").html("")
    if (!url) {
        $(".lyric-inner").html("<p>暂无歌词</p>")
        return
    }
    $.ajax({
        type: "get",
        url: url,
        dataType: 'json',
        success: function (res) {
            console.log(res)
            if(!res.data){
                $(".lyric-inner").html("<p>暂无歌词</p>")
                return
            }
            let str = "";
            for (let i in res.data) {
                str += "<p>" + res.data[i] + "</p>"
            }
            $(".lyric-inner").html(str)
        },
        error: function (err) {
            $(".lyric-inner").html("<p>暂无歌词</p>")
        }
    })
}
//格式化时间
function formatSeconds(value) {
    let result = parseInt(value)
    let h = Math.floor(result / 3600) < 10 ? '0' + Math.floor(result / 3600) : Math.floor(result / 3600);
    let m = Math.floor((result / 60 % 60)) < 10 ? '0' + Math.floor((result / 60 % 60)) : Math.floor((result / 60 % 60));
    let s = Math.floor((result % 60)) < 10 ? '0' + Math.floor((result % 60)) : Math.floor((result % 60));

    let res = '';
    if (h !== '00') res += `${h}:`;
    res += `${m}:`;
    res += `${s}`;
    return res;
}